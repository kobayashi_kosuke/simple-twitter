<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>簡易Twitter</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${empty loginUser}">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${not empty loginUser}">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>
		<c:if test="${not empty loginUser}">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.account}" />
				</div>
				<div class="description">
					<c:out value="${loginUser.description}" />
				</div>
			</div>
		</c:if>
		<c:if test="${not empty errorMessages}">

			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="./" method="get">
			日付：<input type="date" name="start" value=<c:out value = "${start}" />>
			～<input type="date" name="end" value=<c:out value = "${end}" />>
			<input type="submit" value="絞込">
		</form>

		<div class="form-area">
			<c:if test="${isShowMessageForm}">
				<form action="message" method="post">
					いま、どうしてる？<br />
					<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
					<br /> <input type="submit" value="つぶやく">（140文字まで）
				</form>
			</c:if>
		</div>

		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<span class="account"> <a
							href="./?user_id=<c:out value="${message.userId}"/> "> <c:out
									value="${message.account}" /></a>
						</span> <span class="name"><c:out value="${message.name}" /></span>
					</div>
				</div>

				<div class="text">
					<%
						request.setAttribute("newLine", "\n");
					%>
					<c:forEach var="messageNewLine"
						items="${fn:split(message.text, newLine)}">
						<c:out value="${messageNewLine}" />
						<br />
					</c:forEach>
				</div>

				<div class="date">
					<fmt:formatDate value="${message.createdDate}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>


				<div class="deleteMessages">
					<c:if
						test="${not empty loginUser && loginUser.id == message.userId}">
						<form action="deleteMessage" method="post">
							<input type="hidden" name="delete"
								value=<c:out value="${message.id}" />> <input
								type="submit" value="削除">
						</form>
					</c:if>
				</div>
				<div class="editMessages">
					<c:if
						test="${not empty loginUser && loginUser.id == message.userId}">
						<form action="editMessage" method="get">
							<input type="hidden" name="edit"
								value=<c:out value="${message.id}" />> <input
								type="submit" value="編集">
						</form>
					</c:if>
				</div>
				<div class="commentMessages">
					<c:if test="${isShowMessageForm}">
						<form action="commentMessage" method="post">
							<br />
							<textarea name="comment" cols="100" rows="5" class="tweet-box"></textarea>
							<br /> <input type="hidden" name="messageId"
								value=<c:out value="${message.id }" />><br /> <input
								type="submit" value="返信">（140文字まで）
						</form>
					</c:if>
				</div>
				<c:forEach items="${comments}" var="comment">
					<c:if test="${comment.messageId == message.id}">
						<div class="account-name">
							<span class="account"> <c:out value="${comment.account}" />
							</span> <span class="name"><c:out value="${comment.name}" /></span>
						</div>

						<div class="text">
							<%
								request.setAttribute("newLine", "\n");
							%>
							<c:forEach var="commentNewLine"
								items="${fn:split(comment.text, newLine)}">
								<c:out value="${commentNewLine}" />
								<br />
							</c:forEach>
						</div>

						<div class="date">
							<fmt:formatDate value="${comment.createdDate}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
					</c:if>
				</c:forEach>
			</c:forEach>
		</div>
	</div>
	<div class="copyright">Copyright(c)Kobayashi.Kosuke</div>
</body>
</html>