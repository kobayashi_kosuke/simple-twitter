package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns= {"/editMessage"})
public class EditServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String editParam =  request.getParameter("edit");
		Message message = null;
		List<String> errorMessages = new ArrayList<>();

		if(!StringUtils.isEmpty(editParam) && editParam.matches("^[1-9][0-9]*")) {
			int messageId = Integer.parseInt(editParam);
			message = new MessageService().select(messageId);
		}

		if(message == null) {
    		errorMessages.add("不正なパラメータが入力されました");
    		request.getSession().setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
    	}

		request.setAttribute("message", message);
		request.getRequestDispatcher("/edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Message message = new Message();
		message.setText(request.getParameter("text"));
		message.setId(Integer.parseInt(request.getParameter("id")));
		List<String> errorMessages = new ArrayList<>();

		if(!isValid(message, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		new MessageService().update(message);
		response.sendRedirect("./");
	}

	private boolean isValid(Message message, List<String> errorMessages) {
		
		String stringMessage = message.getText();

		if(StringUtils.isBlank(stringMessage)) {
			errorMessages.add("メッセージを入力してください");
		}else if(140 < stringMessage.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if(errorMessages.size() !=0) {
			return false;
		}
		return true;
	}

}
