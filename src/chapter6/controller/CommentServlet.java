package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

@WebServlet(urlPatterns= {"/commentMessage"})
public class CommentServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String comment = request.getParameter("comment");

		List<String> errorMessages = new ArrayList<>();
		if(!isValid(comment,errorMessages)) {
			request.getSession().setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		int messageId = Integer.parseInt(request.getParameter("messageId"));
		User user = (User) request.getSession().getAttribute("loginUser");

		Comment sentence = new Comment();

		sentence.setText(comment);
		sentence.setMessageId(messageId);
		sentence.setUserId(user.getId());

		new CommentService().insert(sentence);
		response.sendRedirect("./");
	}

	private boolean isValid(String reply, List<String> errorMessages) {

		if(StringUtils.isBlank(reply)) {
			errorMessages.add("メッセージを入力してください");
		}else if(140< reply.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if(errorMessages.size() !=0) {
			return false;
		}
		return true;
	}
}
